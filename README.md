# My Auto Devops proof of concept

[![pipeline status](https://gitlab.com/serrodcal/my-autodevops-poc/badges/master/pipeline.svg)](https://gitlab.com/serrodcal/my-autodevops-poc/commits/master)
[![coverage report](https://gitlab.com/serrodcal/my-autodevops-poc/badges/master/coverage.svg)](https://gitlab.com/serrodcal/my-autodevops-poc/commits/master)

This is an small project to test [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) from [GitLab](https://gitlab.com/).

For this purpose, I develop an small project with [Scala](https://www.scala-lang.org/) using [Akka](https://akka.io/) Toolkit (Akka Actor and Akka HTTP).

The idea is to get a pipeline and to deploy using Docker in any PaaS with Kubernetes.
